import state from '@/store/products/state'
import getters from '@/store/products/getters'
import mutations from '@/store/products/mutations'
import actions from '@/store/products/actions'

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}