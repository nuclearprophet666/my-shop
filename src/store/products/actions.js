import productsAPI from '@/api/products'

export default  {
    async fetch({ commit }) {
        const data = await productsAPI.fetch()
        commit('setProducts', data)
    }
}