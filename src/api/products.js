import axios from '@/api'

export default {
    async fetch () {
        try {
            const res = await axios.get('photos', {
                params: {
                    _limit: 10
                }
            });
            const { data } = res;
            return data
        } catch (e) {
            console.log(e)
        }
    }
}